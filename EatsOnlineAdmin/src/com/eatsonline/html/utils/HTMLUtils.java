package com.eatsonline.html.utils;

import java.text.MessageFormat;

public class HTMLUtils {

	public static String createImgLink(final int id, final String function, final String className, final String image, final String title) {
		final String src = "<img src=\"{0}\" style=\"cursor:pointer\" onclick=\"{1}({2}) alt=\"{3}\" class=\"{4}\"/>";
		return MessageFormat.format(src, image, function, id, title, className);
	}
}
