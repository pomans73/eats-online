var dlg = [];

var service_uri = 'http://localhost:8080/webservice/';
var rest_service_uri = service_uri + 'rest/';

function getActualContentHeight(){
	return $(window).height() - $('.page-header').height() - 250;
}


/** for initialize when page is loaded * */
$(document).ready(function() {
	
	var resizeContentBody = function(){
		$('#content').height($(window).height() - $('.page-header').height() - 130);	
	};
	
	$(window).resize(resizeContentBody);
	resizeContentBody();
	
	
	$('#nav > li > a').click(function() {
		if ($(this).attr('class') != 'active') {
			$('#nav li ul').slideUp();
			$(this).next().slideToggle();
			$('#nav li a').removeClass('active');
			$(this).addClass('active');
		}
	});
	
	//menu clicked action
	$('a.link', '#nav').click(function(){

		var target = $(this).attr('data-target');
		$('#content-title').text($(this).text());
		loadMainPanel('contents/' + target + '.jsp', function(responseText, textStatus, XMLHttpRequest){
			
			if(target == 'all-content'){// display front page
				$('.tab-content, .tab-pane', this).height('100%');
				$('.btn-refresh', this).click(function(){
					var iframe = $('iframe', 'div.active');
					iframe.attr('src', iframe.attr('src'));
				});

			}else if(target == 'images-slide'){// manage slide show on home page
				initSlideImages();
			}else if(target == 'manage-shop'){// manage shop show on home page
				initShop();
			}else if(target == 'display-feature-shop'){
				$('#feature-shop-table').flexigrid({

					buttons : [
						{name: 'Add', bclass: 'add', onpress : function(){
							alert('add');
						}},
						{name: 'Delete', bclass: 'delete', onpress : function(){
							alert('delete');
						}},
						{separator: true}
						],
					
					usepager: true,
					rp: 15,
					showTableToggleBtn: true,
					width: '100%',
					height: 400
				});   

			}else if(target == 'manage-users'){
				$('#manage-users-table').flexigrid({
					url: rest_service_uri + 'slide_images',
					dataType: 'json',
					colModel : [
						{display: '#', name : 'seq', width : 40, sortable : true, align: 'center'},
						{display: 'Title', name : 'title', width : 180, sortable : true, align: 'left'},
						{display: 'Description', name : 'description', width : 120, sortable : true, align: 'left'},
						{display: 'URL', name : 'image_path', width : 130, sortable : true, align: 'left', hide: true}
						],
					buttons : [
						{name: 'Add', bclass: 'add', onpress : function(){
							alert('add');
						}},
						{name: 'Delete', bclass: 'delete', onpress : function(){
							alert('delete');
						}},
						{separator: true}
						],
					
					usepager: true,
					rp: 15,
					showTableToggleBtn: true,
					width: '100%',
					height: 400
				});   

			}
			
			//for WYSIWYG Editor
			var editors = ['edit-page-about-us', 'edit-page-draw-prize', 'edit-page-privacy-policy', 'edit-term-conditions'];
			if(editors.indexOf(target) != -1){
				var opts = {
					absoluteURLs: false,
					cssClass : 'el-rte',
					lang     : 'en',
					height   : 420,
					toolbar  : 'maxi'
				};
				$('#editor').elrte(opts);
			}
		});
	});
});


function loadMainPanel(url, callback){
	$('#content').load(url, null, callback);
}