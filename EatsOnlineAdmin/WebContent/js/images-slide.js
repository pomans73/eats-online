var _id = null;
var rows = new Array();

function initSlideImages(){
	 
	 $('#slide-images-table').jtable({
         title: 'Slide Images List',
         paging: true, //Enable paging
         sorting: false, //Enable sorting
         selecting: true, //Enable selecting
         multiselect: true, //Allow multiple selecting
         selectingCheckboxes: true, //Show checkboxes on first column,
         selectionChanged : function(){
        	 var $selectedRows = $('#slide-images-table').jtable('selectedRows');
        	 rows = new Array();
        	 $selectedRows.each(function () {
                 var record = $(this).data('record');
                 rows.push(record.id);
             });
         },
         actions: { 
             listAction: rest_service_uri + 'slide_images/search'
         },
         fields: {
        	 seq : {
        		 title : '#',
        		 width: '5%'
        	 },
             id: {
                 title: 'ID',
                 key: true,
                 width: '5%',
             },
             title: {
                 title: 'Title',
                 width: '15%',
             },
             description: {
                 title: 'Description',
                 width: '15%'
             },
             image: {
                 title: 'Image',
                 width: '20%'
             },
             published: {
                 title: 'Status',
                 width: '20%',
                 options: { 'Y': 'Published', 'N': 'Pending' }
             },
             action: {
                 title: 'Action',
                 width: '20%'
             }
         }
     });

	 $('.slide-images-search').click(function(){
		 $('#slide-images-table').jtable('load', {keyword : $('#txt-search').val()});
	 });

	dlg[0] = $('#upload-dialog').dialog({
		autoOpen : false,
		height : 400,
		width : 700,
		modal : true,
		buttons : {
			"Save" : saveSildeImages ,
			Cancel : function() {
				$(this).dialog('close');
			}
		},
		open : function(){
			//clear selected file in input:file
			var tmp = $('#browse-img', dlg[0]).parent().html();
			var _parent = $('#browse-img', dlg[0]).parent();
			_parent.html(tmp);
			$('#preview', dlg[0]).hide();
			//bind event for input:file for preview selected image
			$('#browse-img', dlg[0]).change(function(){
				$('#frm', dlg[0]).attr('action', service_uri + 'upload-files.html').submit();
				$('#upload-frm').load(function(){
					if($(this).contents().text() == 'OK'){
						//view image from session
						$('#preview', dlg[0]).width($('#img-container', dlg[0]).width()- 10).attr('src', service_uri + 'view_image.bin?file_name=upload-file').show();	
					}
				});	
			});
		},
		close : function() {
			//flush image from session
			$.get(service_uri + 'view_image.bin', {file_name:'upload-file', flush:true});
			//clear form
			$('input:text, textarea', this).val('');
			$('#preview', this).attr('src', '').addClass('no-show');
		}
	});
}

function addSlideImages(com, grid) {
	
	dlg[0].dialog('open');
	$('.dlg-mode', dlg[0]).text('New');
	$('.ui-button:contains(Save)').show();
	$('.file-upload', dlg[0]).show();
	
	

}

function editSlide(id){
	_id = id; 
	$.getJSON(rest_service_uri + 'slide_images?id=' + id, {}, function(data){
		if(data.length > 0){
			$('#img-title', dlg[0]).val(data[0].title);
			$('#img-desc', dlg[0]).val(data[0].description); 
			$.getJSON(service_uri + 'view_image.bin?file_name=upload-file&type=slide_images&id=' + data[0].img_ref_id, {}, function(data){
				if(data){
					dlg[0].dialog('open');
					$('.ui-button:contains(Save)').show();
					$('.file-upload', dlg[0]).show();
					$('.dlg-mode', dlg[0]).text('Update');
					$('#preview', dlg[0]).width($('#img-container', dlg[0]).width()- 10).attr('src', service_uri + 'view_image.bin?file_name=upload-file').show();	
					
				}
			});
		}
		
	});
	
}

function viewImageSlide(id){
	$('<div title="Preview"><img class="img-preview" src="' + service_uri + 'view_image.bin?viewImage=true&type=slide_images&id=' + id + '"/></div>').dialog({
		width : 700,
		height : 500,
		modal : true,
		resize: true,
		buttons : {
			Close : function() {
				$(this).dialog('close');
			}
		}
	});
}

function viewSlide(id){
	_id = id;
	$.getJSON(rest_service_uri + 'slide_images?id=' + id, {}, function(data){
		if(data.length > 0){
			$('#img-title', dlg[0]).val(data[0].title);
			$('#img-desc', dlg[0]).val(data[0].description);
			$.getJSON(service_uri + 'view_image.bin?file_name=upload-file&type=slide_images&id=' + data[0].img_ref_id, {}, function(data){
				if(data){
					dlg[0].dialog('open');
					$('.ui-button:contains(Save)').hide();
					$('.file-upload', dlg[0]).hide();
					$('.dlg-mode', dlg[0]).text('View');
					$('#preview', dlg[0]).width($('#img-container', dlg[0]).width()- 10).attr('src', service_uri + 'view_image.bin?file_name=upload-file').show();	
				}
			});
		}
		
	});
}

function publishSlide(id, publish){
	_id = id;
	var slideData = [
		{name : 'published' , value : !publish ? 'Y' : publish},
		{name : 'id' , value : _id}
	];
	$.ajax({
		url: rest_service_uri + 'slide_images/update',
		type: 'PUT',
		data : slideData,
		encoding: 'UTF-8',
		contentType: "application/json; charset=utf-8",
		dataType:'json',
		success : function(data){
			if(data.result == 'OK'){
				$('#slide-images-table').jtable('load', {keyword : $('#txt-search').val()});
			}
		},
		error : function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		}
	});
}

function unPublishSlide(id){
	publishSlide(id, 'N');
}

function saveSildeImages() {
	$.getJSON(service_uri + 'view_image.bin?file_name=upload-file&data=base64', {}, function(data){
		var img = data;
		
		var slideData = null;
		if($('.dlg-mode', dlg[0]).text() == 'New'){
			slideData = [
		 			    {name : 'title', value : $('#img-title', dlg[0]).val()},
						{name : 'description' , value : $('#img-desc', dlg[0]).val()},
						{name : 'file_name' , value : img.filename},
						{name : 'file_data' , value : img.data},
						{name : 'content_type' , value : img.content_type}
						];
		}else{
			slideData = [
		 			    {name : 'title', value : $('#img-title', dlg[0]).val()},
						{name : 'description' , value : $('#img-desc', dlg[0]).val()},
						{name : 'file_name' , value : img.filename},
						{name : 'file_data' , value : img.data},
						{name : 'content_type' , value : img.content_type},
						{name : 'id' , value : _id}
						];
		}
		
		$.ajax({
			url: rest_service_uri + ($('.dlg-mode', dlg[0]).text() == 'New' ? 'slide_images/insert' : 'slide_images/update'),
			type: 'PUT',
			data : slideData,
			encoding: 'UTF-8',
			contentType: "application/json; charset=utf-8",
			dataType:'json',
			success : function(data){
				if(data.result == 'OK'){
					$('#slide-images-table').jtable('load', {keyword : $('#txt-search').val()});
					$(dlg[0]).dialog("close");
				}
			},
			error : function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			}
		});
	});
}

function removeSlideImages(com, grid){
	

	if(rows.length == 0 || !confirm('Confirm to remove ID ' + rows)){
		return;
	}
	
	
	$.ajax({
		url: rest_service_uri + 'slide_images?id=' + rows,
		type: 'DELETE',
		contentType: "application/json",
		dataType:'json',
		success : function(data){
			if(data.result == 'OK'){
				$('#slide-images-table').jtable('load');
				$(dlg[0]).dialog("close");
			}
		},
		error : function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		}
	});
}