var _id = null;


function initShop(){
	$('#manage-shop-table').flexigrid({
		url: rest_service_uri + 'shop',
		dataType: 'json',
		method: 'GET',
		
		colModel : [
			{display: '#', name : 'seq', width : 40, sortable : true, align: 'center'},
			{display: 'ID', name : 'id', width : 40, sortable : true, align: 'center'},
			{display: 'Shop Name', name : 'shop_name', width : 200, sortable : true, align: 'left'},
			{display: 'Cuisine', name : 'shop_cuisine', width : 100, sortable : true, align: 'left'},
			{display: 'Suburb', name : 'suburb', width :80, sortable : false, align: 'center'},
			{display: 'City', name : 'city', width : 80, sortable : true, align: 'center'},
			{display: 'Country', name : 'country', width : 80, sortable : true, align: 'center'},
			{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
			{display: 'Action', name : 'action', width : 80, sortable : false, align: 'center'}
			
		],
		searchitems : [
                       {display: 'Shop Name', name : 'shop_name', isdefault: true},
                       {display: 'City', name : 'city'}
               ],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : addShop},
			{name: 'Delete', bclass: 'delete', onpress : removeShop},
			{separator: true}
		],
		
		usepager: true,
		rp: 15,
		resizable: false,
		showTableToggleBtn: true,
		width: '100%',
		height: getActualContentHeight()
	});   
	
}

function addShop(){
	loadMainPanel('contents/shop-entry.jsp', function(responseText, textStatus, XMLHttpRequest){

		initShopEntry();
	});
}

function removeShop(){
	loadMainPanel('contents/shop-entry.jsp', function(responseText, textStatus, XMLHttpRequest){
		
	});
}

function editShop(id){
	loadMainPanel('contents/shop-entry.jsp', function(responseText, textStatus, XMLHttpRequest){
		
	});
}

function viewShop(id){
	loadMainPanel('contents/shop-entry.jsp', function(responseText, textStatus, XMLHttpRequest){
		
	});
}