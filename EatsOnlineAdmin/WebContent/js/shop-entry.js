var _currentImg;

function initShopEntry(){
	
	$('label').each(function(){
		if($(this).next().is('input:not([readonly])')){
			$(this).next().attr('placeholder', 'Enter ' + $(this).text().toLowerCase());	
		}
	});
	

	
	$('#shop_loc_search').geocomplete({
		map: '#gmap',
		details: 'div',
		detailsAttribute: 'data-geo',
		
	});
	
	$("button.find").click(function(){
	  $('#shop_loc_search').trigger("geocode");
	});
	
	$('.choose-img').each(function(){
		var bttTools = $('<div class="img-choose-btt" style="margin-top: 10px"/>');
		$('<button class="btn btn-danger">Choose image</button>').click(function(){
			dlg[0].dialog('open');
			_currentImg = $(this).closest('.column').find('.choose-img');
			dlg[0].attr('ref-id', _currentImg.attr('id'));
		}).appendTo(bttTools);
		bttTools.appendTo($(this).parent());
	});
	
	dlg[0] = $('#choose-img-dialog').dialog({
		autoOpen : false,
		height : 350,
		width : 700,
		modal : true,
		buttons : {
			OK : chooseShopImg ,
			Cancel : function() {
				$(this).dialog('close');
			}
		},
		open : function(){
			//clear selected file in input:file
			var tmp = $('#browse-img', dlg[0]).parent().html();
			var _parent = $('#browse-img', dlg[0]).parent();
			_parent.html(tmp);
			$('#preview', dlg[0]).hide();
			//bind event for input:file for preview selected image
			$('#browse-img', dlg[0]).change(function(){
				$('#browse-img', dlg[0]).attr('name', dlg[0].attr('ref-id'));
				$('#frm', dlg[0]).attr('action', service_uri + 'upload-files.html').submit();
				$('#upload-frm').load(function(){
					if($(this).contents().text() == 'OK'){
						//view image from session
						$('#preview', dlg[0]).width($('#img-container', dlg[0]).width()- 10).attr('src', service_uri + 'view_image.bin?file_name=' + dlg[0].attr('ref-id')).show();	
					}
				});	
			});
		},
		close : function() {
			//flush image from session
			$.get(service_uri + 'view_image.bin', {file_name: dlg[0].attr('ref-id'), flush:true});
			$('#preview', this).attr('src', '').addClass('no-show');
		}
	});
	
	
	$('#shop-menu-table').flexigrid({
		url: rest_service_uri + 'shop',
		dataType: 'json',
		method: 'GET',
		
		colModel : [
			{display: '#', name : 'seq', width : 40, sortable : true, align: 'center'},
			{display: 'ID', name : 'id', width : 40, sortable : true, align: 'center'},
			{display: 'Menu', name : 'shop_name', width : 200, sortable : true, align: 'left'},
			{display: 'Type', name : 'shop_cuisine', width : 100, sortable : true, align: 'left'},
			{display: 'Price', name : 'shop_cuisine', width : 100, sortable : true, align: 'right'},
			{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
			{display: 'Action', name : 'action', width : 80, sortable : false, align: 'center'}
			
		],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : addShop},
			{name: 'Delete', bclass: 'delete', onpress : removeShop},
			{separator: true}
		],
		
		usepager: true,
		rp: 15,
		resizable: false,
		showTableToggleBtn: true,
		width: '100%',
		height: getActualContentHeight()
	});   
	
	
	$('#shop-gallery-table').flexigrid({
		url: rest_service_uri + 'slide_images',
		dataType: 'json',
		method: 'GET',
		
		colModel : [
			{display: '#', name : 'seq', width : 20, sortable : true, align: 'center'},
			{display: 'ID', name : 'id', width : 20, sortable : true, align: 'center'},
			{display: 'Title', name : 'title', width : 200, sortable : true, align: 'left'},
			{display: 'Image', name : 'image', width : 200, sortable : false, align: 'center'},
			{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
			{display: 'Action', name : 'action', width : 80, sortable : false, align: 'center'}
			
		],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : addSlideImages},
			{name: 'Delete', bclass: 'delete', onpress : removeSlideImages},
			{separator: true}
		],
		
		usepager: true,
		rp: 15,
		resizable: false,
		showTableToggleBtn: true,
		width: '100%',
		height: getActualContentHeight()
	}); 
	
	$(function() {
	    $('.tabbable').bind('click', function (e) {
	        $('.gBlock').hide();
	    });
	});
	
	//for WYSIWYG Editor	
	var opts = {
		absoluteURLs: false,
		cssClass : 'el-rte',
		lang     : 'en',
		height   : getActualContentHeight(),
		toolbar  : 'maxi'
	};
	$('#editor').elrte(opts);

	$('#shop-review-table').flexigrid({
		url: rest_service_uri + 'slide_images',
		dataType: 'json',
		method: 'GET',
		
		colModel : [
			{display: '#', name : 'seq', width : 20, sortable : true, align: 'center'},
			{display: 'ID', name : 'id', width : 20, sortable : true, align: 'center'},
			{display: 'Customer name', name : 'image', width : 150, sortable : false, align: 'center'},
			{display: 'Title', name : 'title', width : 200, sortable : true, align: 'left'},
			{display: 'Rating', name : 'image', width : 80, sortable : false, align: 'center'},
			{display: 'Status', name : 'status', width : 80, sortable : true, align: 'center'},
			{display: 'Action', name : 'action', width : 80, sortable : false, align: 'center'}
			
		],
		buttons : [
			{name: 'Add', bclass: 'add', onpress : addSlideImages},
			{name: 'Delete', bclass: 'delete', onpress : removeSlideImages},
			{separator: true}
		],
		
		usepager: true,
		rp: 15,
		resizable: false,
		showTableToggleBtn: true,
		width: '100%',
		height: getActualContentHeight()
	}); 
}

function chooseShopImg(){
	_currentImg.attr('src', service_uri + 'view_image.bin?file_name=' + dlg[0].attr('ref-id'));
	dlg[0].dialog('close');
}

function backToSearchShop(){
	loadMainPanel('contents/manage-shop.jsp', initShop);
}

function saveShop(){
	
	if(!confirm('Confirm to save')){
		return;
	}
	
	var shopData = new Array();
	
	$('input:text, input[type="number"], textarea[name="shop_description"]', '.shop-entry').each(function(){
		var val = {};
		val.name = this.name;
		val.value = this.name == 'shop_description' ? $(this).elrte('val') : this.value;
		shopData.push(val);
	});
	$.ajax({
		url: rest_service_uri + ($('.shop-mode', '.shop-entry').text() == 'New' ? 'shop/insert' : 'shop/update'),
		type: 'PUT',
		data : shopData,
		encoding: 'UTF-8',
		contentType: "application/json; charset=utf-8",
		dataType:'json',
		success : function(data){
			if(data.result == 'OK'){
				backToSearchShop();
			}
		},
		error : function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		}
	});
}