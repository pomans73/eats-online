<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Eats online system administrator page</title>
<jsp:include page="includes/header.jsp" flush="true" />
</head>

<header class="top-header page-header blue-shade">
	<h1 class="sys-title">
		<img src="images/logo.png" height="30px" /> 
		<small style="color:#000000">Eats online system administrator page </small>
	</h1>
	<div class="ui-widget-header">&nbsp;</div>
</header>

<div class="row">
	<div class="col-md-10 col-md-push-2">
		<section class="panel panel-default" id="content-section" style="padding:0px">
			<div class="panel-heading" >
				<h3 class="panel-title" id="content-title">Panel title</h3>
			</div>
			<div style="padding:5px">
				<div class="panel-body" id="content" style="height: 400px; overflow: scroll">Panel content</div>
			</div>
		</section>
	</div>
	<div class="col-md-2 col-md-pull-10">

		<nav id="left-menu">
			<ul id="nav">
				<li><a href="main.jsp">Home</a></li>
				<li><a href="javascript:void(0)" class="link" data-target="global-config">Global Configuration</a></li>
				<li><a href="javascript:void(0)">Manage Web Content</a>
					<ul>
						<li><a href="javascript:void(0)" class="link" data-target="all-content">
							Show All Content
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="images-slide">
							Images Slide
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="display-feature-shop">
							Display Feature Shop
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="edit-page-about-us">
							Edit Page About Us
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="edit-page-draw-prize">
							Edit Page Draw Prize
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="edit-page-privacy-policy">
							Edit Page Privacy Policy
						</a></li>
						<li><a href="javascript:void(0)" class="link" data-target="edit-term-conditions">
							Edit Page Term &amp; Conditions
						</a></li>
					</ul></li>

				<li><a href="javascript:void(0)" class="link" data-target="manage-users">Manage Users</a></li>
				<li><a href="javascript:void(0)" class="link" data-target="manage-shop">Manage Shop</a></li>
			</ul>
		</nav>


	</div>
</div>







<footer> </footer>
<jsp:include page="includes/footer.jsp" flush="true" />
<script type="text/javascript" lang="javascript" src="js/admin.js"></script>


</body>
</html>