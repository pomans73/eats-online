<%@include file="function.jsp" %>
<title>
	EatsOnline : Administrator page
</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" >
<link rel="icon" type="image/png"  href="images/favicon/76x76.png">
<link rel="apple-touch-icon" href="images/favicon/76x76.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/152x152.png">
<%
includeCSS(application, out, "bootstrap/css");
includeCSS(application, out, "js/themes/base");
includeCSS(application, out, "elrte/css");
includeCSS(application, out, "flexigrid/css");
includeCSS(application, out, "splitpane");
includeCSS(application, out, "jtable.2.3.1/themes/metro/blue");
%>
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/manage-shop.css" />
<link rel="stylesheet" type="text/css" href="css/shop-entry.css" />