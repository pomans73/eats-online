<%@include file="function.jsp" %>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" lang="javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" lang="javascript" src="js/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" lang="javascript" src="js/jquery.geocomplete.js"></script>
<%
includeJS(application, out, "bootstrap/js");
includeJS(application, out, "js/ui");
includeJS(application, out, "elrte/js");
includeJS(application, out, "flexigrid/js");
includeJS(application, out, "splitpane");
includeJS(application, out, "jtable.2.3.1");
%>
<script type="text/javascript" lang="javascript" src="js/images-slide.js"></script>
<script type="text/javascript" lang="javascript" src="js/manage-shop.js"></script>
<script type="text/javascript" lang="javascript" src="js/shop-entry.js"></script>