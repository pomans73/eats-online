<%@page import="java.io.File"%>
<%!
public static void includeCSS(final ServletContext application, final JspWriter out, final String path) throws Exception{
		final File jsUI = new File(application.getRealPath(path));
		for (final File child : jsUI.listFiles())
			if (child.isFile() && child.getName().endsWith(".css"))
				out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + path + "/" + child.getName() + "\"/>");
}

public static void includeJS(final ServletContext application, final JspWriter out, final String path) throws Exception{
	final File jsUI = new File(application.getRealPath(path));
	for(final File child : jsUI.listFiles())
		if(child.isFile() && child.getName().endsWith(".js"))
			out.println("<script type=\"text/javascript\" lang=\"javascript\" src=\"" + path + "/" + child.getName() + "\"></script>");
}
%>