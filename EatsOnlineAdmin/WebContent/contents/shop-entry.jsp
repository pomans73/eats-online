

<div class="container shop-entry" style="width:100%">
<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
		 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
		 
		 <a class="navbar-brand" href="#"><span class="shop-mode">New</span> Shop</a>
	</div>
	
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<div class="navbar-form navbar-right" >
		<button type="button" class="btn btn-danger" onclick="saveShop()">Save</button>
		<button type="button" class="btn btn-default" onclick="backToSearchShop()">Cancel</button>
		</div>
	</div>
	
</nav>
	<div class="row clearfix">

		<div class="col-xs-12 column">
		
			<div class="row clearfix">
				<div class="col-xs-12 column">
					<label for="shop_loc_position">Shop map location</label>
					<div class="input-group">
						 <input type="text" class="form-control" id="shop_loc_search" name="shop_loc_search" />
						 <span class="input-group-btn">
					     	<button class="find btn btn-danger" type="button">Search</button>
					     </span>
					</div>
					<br/>
					<div class="row clearfix">
						<div class="col-xs-6 column">
							<label for="shop_loc_position">Map</label>	
							<div id="gmap" class="col-xs-12 column" style="height: 300px; display:block"></div>
						</div>
						<div class="col-xs-6 column">
							<div class="input-group">
								<label for="shop_loc_position">Latitude, Longitude position</label>
								<input type="text" class="form-control" readonly="readonly" data-geo="location" name="shop_loc_position" id="shop_loc_position"/>
							</div>
						</div>
					</div>
					
				
				</div>
			</div>
			<br/>
			<div class="form-group">
				 <label for="shop_name">Shop name</label>
				 <input type="text" class="form-control" id="shop_name" name="shop_name"  data-geo="name"/>
			</div>
			<div class="row clearfix">
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop street no</label> <input type="text"
							class="form-control" id="street_no" name="street_no" data-geo="street_number"/>
					</div>
				</div>
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop street</label> <input type="text"
							class="form-control" id="street_name" name="street_name" data-geo="route"/>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop suburb</label> <input type="text"
							class="form-control" id="suburb" name="suburb" data-geo="administrative_area_level_2"/>
					</div>
				</div>
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop city</label> <input type="text"
							class="form-control" id="city" name="city" data-geo="administrative_area_level_1"/>
					</div>
				</div>
				
			</div>
			<div class="row clearfix">
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop country</label> <input type="text"
							class="form-control" id="country" name="country" data-geo="country"/>
					</div>
				</div>
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop Telephone</label> <input type="text"
							class="form-control" id="shop_tel" name="shop_tel" data-geo="formatted_phone_number"/>
					</div>
				</div>
			</div>
			
			<div class="row clearfix">
				
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Shop URL</label> <input type="text"
							class="form-control" id="shop_url" name="shop_url" data-geo="website"/>
					</div>
				</div>
				<div class="col-xs-6 column">
					<div class="form-group">
						<label for="shop_name">Discount rate</label> 
						<div>
							<input type="number" class="form-control" id="shop_discount_rate" name="shop_discount_rate" value="0.00" step="0.1" style="width: 100px; text-align: center; display: inline-block"/>%
						</div> 
					</div>
				</div>
			</div>
			<br/>
			<div class="row clearfix">
				<div class="col-xs-12 column">
					<label for="hl-img">Shop hilight image</label>
					<img alt="" id="hl-img" src="images/placeholder.gif" class="hl-img img-rounded choose-img" style="display: block" />
				</div>
			</div>
			<br/>
			
			<div class="row clearfix">
				<div class="col-xs-12 column">
					<label for="main-img">Shop main image</label>
					<img alt="" id="main-img" src="images/placeholder.gif" class="main-img img-rounded choose-img" style="display: block" />
				</div>
			</div>
			<br/>
			
			<div class="row clearfix">
				<div class="col-xs-12 column">
					<div class="tabbable" id="tabs-518873">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#panel-menu" data-toggle="tab">Menu</a>
							</li>
							<li>
								<a href="#panel-gallery" data-toggle="tab">Gallery</a>
							</li>
							<li>
								<a href="#panel-description" data-toggle="tab">Description</a>
							</li>
							<li>
								<a href="#panel-review" data-toggle="tab">Review</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="panel-menu">
								<table class="flexigrid" id="shop-menu-table">
									<thead>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="tab-pane" id="panel-gallery">
								<table class="flexigrid" id="shop-gallery-table">
									<thead>

									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="tab-pane" id="panel-description">
								<textarea id="editor" name="shop_description"></textarea>
							</div>
							<div class="tab-pane" id="panel-review">
								<table class="flexigrid" id="shop-review-table">
									<thead>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="choose-img-dialog" title="Choose Image" class="no-show">
	<div class="row">
		<div class="col-xs-6 col-xs-push-6">
			<h4>Image preview</h4>
			<div class="content" id="img-container" style="width: 100%; height: 100%; overflow: auto">
				<img id="preview" class="no-show img-thumbnail" />
			</div>
		</div>
		<div class="col-xs-6 col-xs-pull-6">
			<h4>Choose image</h4>
			<div class="content">
				<form id="frm"
					enctype="multipart/form-data" method="post" target="upload-frm">
				
					<div class="form-group file-upload">
						<label for="browse-img">Browse File</label>
						<span>
							<input type="file" accept="image/*" id="browse-img" name="upload-file" class="form-control" />
						</span>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div>

<iframe id="upload-frm" name="upload-frm" class="no-show"></iframe>
