
<div class="row clearfix">
	<div class="col-xs-12 column">
		<label for="txt-search">Search</label>
		<div class="input-group">
			<input type="text" class="form-control" id="txt-search"
				name="txt-search" /> <span class="input-group-btn">
				<button class="btn btn-danger slide-images-search" type="button">Search</button>
			</span>
		</div>
	</div>
</div>
<br/>
<div class="row clearfix">
	<div class="col-xs-12 column">
		<button class="btn btn-default" onclick="addSlideImages()">Add</button>
		<button class="btn btn-default" onclick="removeSlideImages()">Remove</button>
	</div>
</div>
<br/>
<div id="slide-images-table"></div> 

<div id="upload-dialog" title="Slide Image" class="no-show">
	<div class="row">
		<div class="col-md-6 col-md-push-6">
			<h4>Image preview</h4>
			<div class="content" id="img-container" style="width: 100%; height: 100%; overflow: auto">
				<img id="preview" class="no-show img-thumbnail" />
			</div>
		</div>
		<div class="col-md-6 col-md-pull-6">
			<h4><span class="dlg-mode">New</span> slide image</h4>
			<div class="content">
				<form id="frm"
					enctype="multipart/form-data" method="post" target="upload-frm">
				
					<div class="form-group file-upload">
						<label for="browse-img">Browse File</label>
						<span>
						<input type="file" accept="image/*" id="browse-img" name="upload-file" class="form-control" />
						</span>
					</div>
					<div class="form-group">
						<label for="img-title">Title</label><input id="img-title" type="text" size="30" class="form-control" />
					</div>
					<div class="form-group">
						<label for="img-desc">Description</label>
						<textarea rows="5" cols="30" id="img-desc" class="form-control" ></textarea>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>

<iframe id="upload-frm" name="upload-frm" class="no-show"></iframe>