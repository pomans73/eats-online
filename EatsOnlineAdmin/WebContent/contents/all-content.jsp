
<button type="button" class="btn btn-success btn-refresh" style="float:right">Refresh</button>

<ul class="nav nav-tabs">
	<li><a href="#w320" data-toggle="tab">320px</a></li>
	<li><a href="#w640" data-toggle="tab">640px</a></li>
	<li class="active"><a href="#wFull" data-toggle="tab">Full Size</a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane" id="w320">
		<iframe id="front-web-frm1" src="/index.jsp" width="320px"
			height="100%"> </iframe>
	</div>
	<div class="tab-pane" id="w640">
		<iframe id="front-web-frm2" src="/index.jsp" width="640px"
			height="100%"> </iframe>
	</div>
	<div class="tab-pane active" id="wFull">
		<iframe id="front-web-frm3" src="/index.jsp" width="100%"
			height="100%"> </iframe>
	</div>
</div>
