package com.eatsonline.io.objects;

import java.io.Serializable;
import java.util.Date;

public class SerializableFileObject implements Serializable {

	private static final long serialVersionUID = 3405694950148031912L;
	
	private String fileName;
	private String contentType;
	private String b64Data;
	private long size;
	private Date createDate;
	
	public SerializableFileObject(){
		super();
		setCreateDate(new Date());
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getB64Data() {
		return b64Data;
	}
	public void setB64Data(String b64Data) {
		this.b64Data = b64Data;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
