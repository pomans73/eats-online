package com.eatsonline.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.eatsonline.common.utils.ConfigUtils;
import com.eatsonline.io.objects.SerializableFileObject;
import com.eatsonline.services.rest.common.ImagesStore;
import com.eatsonline.services.rest.common.objects.MapObject;

/**
 * Servlet implementation class ViewImageFromSession
 */
@WebServlet("/view_image.bin")
public class ViewImageFromSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewImageFromSession() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final String type = request.getParameter("type");
		
		if("true".equals(request.getParameter("flush"))){
			request.getSession().removeAttribute(request.getParameter("file_name") + "_object");
			System.out.println("file was flush");
			response.getWriter().print("OK");
		} if(request.getParameter("id") != null){
			
			
			final ArrayList<MapObject> result = new ArrayList<MapObject>();
			final ImagesStore restful = new ImagesStore();
			result.addAll(restful.get(Integer.parseInt(request.getParameter("id")), null));
			
			if(result != null && !result.isEmpty()){
				if(result.size() > 0){
					final MapObject data = result.get(0);
					
					
					final SerializableFileObject file = new SerializableFileObject();
					file.setFileName(data.getString("file_name"));
					file.setContentType(data.getString("content_type"));
					
					final File folder = new File(ConfigUtils.DOCUMENT_PATH  + File.separator + "images_store" + File.separator  + type);
					
					
					final byte[] fileData = FileUtils.readFileToByteArray(new File(folder.getAbsolutePath() + File.separator + file.getFileName()));
					file.setB64Data(Base64.encodeBase64String(fileData));
					file.setSize(fileData.length);
					request.getSession().setAttribute(request.getParameter("file_name") + "_object", file);
					if("true".equals(request.getParameter("viewImage"))){
						response.setContentType(file.getContentType());
						IOUtils.write(Base64.decodeBase64(file.getB64Data()), response.getOutputStream());
					}else{
						response.setContentType("application/json");
						data.put("content_type", file.getContentType());
						data.put("data", file.getB64Data());
						IOUtils.write(JSONObject.wrap(data).toString(), response.getOutputStream());
					}
				}
			}

		}else{
			final SerializableFileObject file = (SerializableFileObject)request.getSession().getAttribute(request.getParameter("file_name") + "_object");
			if("base64".equals(request.getParameter("data"))){
				if(file != null && file.getSize() > 0){
					response.setContentType("application/json");
					final JSONObject json = new JSONObject();
					try {
						json.put("filename", file.getFileName());
						json.put("data", file.getB64Data());
						json.put("content_type", file.getContentType());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					IOUtils.write(json.toString(), response.getOutputStream());
				}
			}else{
				if(file != null && file.getSize() > 0){
					response.setContentType(file.getContentType());
					IOUtils.write(Base64.decodeBase64(file.getB64Data()), response.getOutputStream());
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
