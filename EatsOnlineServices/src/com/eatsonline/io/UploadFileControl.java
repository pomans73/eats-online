package com.eatsonline.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.eatsonline.io.objects.SerializableFileObject;

/**
 * Servlet implementation class UploadFileControl
 */
@WebServlet("/upload-files.html")
public class UploadFileControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadFileControl() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
	        List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	        final HttpSession session = request.getSession();
	        for (FileItem item : items) {
	            if (item.isFormField()) {
	                // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
	                //String fieldname = item.getFieldName();
	                //String fieldvalue = item.getString();
	                // ... (do your job here)
	            } else {
	                // Process form file field (input type="file").
	                final String fieldname = item.getFieldName();
	                final String filename = FilenameUtils.getName(item.getName());
	                System.out.println("file : " + filename);
	                InputStream filecontent = item.getInputStream();
	                final byte[] b = new byte[filecontent.available()];
	                filecontent.read(b);
	                filecontent.close();	  
	                final String b64Data = Base64.encodeBase64String(b);
	                
	                final SerializableFileObject file = new SerializableFileObject();
	                file.setB64Data(b64Data);
	                file.setFileName(filename);
	                file.setContentType(item.getContentType());
	                file.setSize(b.length);
	                
	                session.setAttribute(fieldname + "_object", file);
	                response.getWriter().print("OK");
	            }
	        }
	    } catch (FileUploadException e) {
	        throw new ServletException("Cannot parse multipart request.", e);
	    }
	}

}
