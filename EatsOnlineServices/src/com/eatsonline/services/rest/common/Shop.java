package com.eatsonline.services.rest.common;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.json.JSONException;
import org.json.JSONObject;

import com.eatsonline.common.utils.ConfigUtils;
import com.eatsonline.html.utils.HTMLUtils;
import com.eatsonline.services.db.utils.DSUtils;
import com.eatsonline.services.rest.common.objects.MapObject;

@Path("shop")
public class Shop extends AbstractRestful{
    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest req;

    /**
     * Default constructor. 
     */
    public Shop() {
    	super();
    }

    /**
     * Retrieves representation of an instance of Users
     * @return an instance of String
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public ArrayList<MapObject> get(@QueryParam(value="id") final Integer id, @QueryParam(value="published") final String published) {
    	ArrayList<MapObject> result =  null;
    	try {

			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			final ResultSetHandler<ArrayList<MapObject>> handler = new ResultSetHandler<ArrayList<MapObject>>(){
				@Override
				public ArrayList<MapObject> handle(ResultSet rs) throws SQLException {
					final ArrayList<MapObject> list = new ArrayList<MapObject>(); 
					while(rs.next()){
						final MapObject row = new MapObject();
						final int id = rs.getInt("id");
						row.put("seq", list.size() + 1);
						final ResultSetMetaData rsm = rs.getMetaData();
						int colCount = rsm.getColumnCount();
						for(int i = 1; i <= colCount; i++){
							row.put(rsm.getColumnName(i), rs.getObject(i));
						}
						
						boolean published = "Y".equals(rs.getString("published"));
						row.put("shop_image", HTMLUtils.createImg(id, "shop-img", ConfigUtils.SERVICE_URI + "view_image.bin?id=" + id + "&type=shop&viewImage=true", rs.getString("shop_name")));
						row.put("status", published ? "Published" : "Pending");
						row.put("action", 
								HTMLUtils.createImgLink(id, "editShop", "", "images/icon/page_white_edit.png", "Edit") + "&nbsp;" + 
								HTMLUtils.createImgLink(id, "viewShop", "", "images/icon/find.png", "View") + "&nbsp;" + 
								HTMLUtils.createImgLink(id, published ? "unPublishShop" :"publishShop", "", published ? "images/icon/cross.png" : "images/icon/accept.png", "Publish"));
						
						list.add(row);
					}
					return list;
				}
			};
			final StringBuffer sql = new StringBuffer();
			sql.append("select * from shop where 1 = 1 ");
			if(id != null){
				sql.append("and id = ? ");
			}
			if("true".equals(published)){
				sql.append("and published = 'Y' ");
			}
			sql.append("order by id");
			if(id != null){
				result = query.query(sql.toString(), handler, id);
			}else{
				result = query.query(sql.toString(), handler);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    

    /**
     * PUT method for updating or creating an instance of Users
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{action}")
    public String insertOrUpdate(@PathParam(value="action") final String action, 
			@FormParam(value = "shop_name") final String shop_name,
			@FormParam(value = "shop_cuisine") final String shop_cuisine,
			@FormParam(value = "shop_tel") final String shop_tel,
			@FormParam(value = "shop_url") final String shop_url,
			@FormParam(value = "shop_rating") final Double shop_rating,
			@FormParam(value = "shop_discount_rate") final Double shop_discount_rate,
			@FormParam(value = "shop_loc_position") final String shop_loc_position,
			@FormParam(value = "street_no") final String street_no,
			@FormParam(value = "street_name") final String street_name,
			@FormParam(value = "suburb") final String suburb,
			@FormParam(value = "city") final String city,
			@FormParam(value = "country") final String country,
			
			@FormParam(value = "shop_description") final String shop_description,
			
			@FormParam(value = "published") final String published,
			
			@FormParam(value = "main_file_name") final String main_file_name,
			@FormParam(value = "main_content_type") final String main_content_type,
			@FormParam(value = "main_image_data") final String main_image_data,
			
			@FormParam(value = "hl_file_name") final String hl_file_name,
			@FormParam(value = "hl_content_type") final String hl_content_type,
			@FormParam(value = "hl_image_data") final String hl_image_data,
			
			
			@FormParam(value = "id") final String idUpdate

    		) throws JSONException {
    	final JSONObject response = new JSONObject();
    	try {
    		
			JSONObject data = new JSONObject();
			 
			DSUtils.paramToJSONObject(data, "shop_name", shop_name);
			DSUtils.paramToJSONObject(data, "shop_description", shop_description);
			DSUtils.paramToJSONObject(data, "shop_cuisine", shop_cuisine);
			DSUtils.paramToJSONObject(data, "shop_tel", shop_tel);
			DSUtils.paramToJSONObject(data, "shop_url", shop_url);
			DSUtils.paramToJSONObject(data, "shop_rating", shop_rating);
			DSUtils.paramToJSONObject(data, "shop_discount_rate", shop_discount_rate);
			DSUtils.paramToJSONObject(data, "shop_loc_position", shop_loc_position);
			DSUtils.paramToJSONObject(data, "street_no", street_no);
			DSUtils.paramToJSONObject(data, "street_name", street_name);
			DSUtils.paramToJSONObject(data, "suburb", suburb);
			DSUtils.paramToJSONObject(data, "city", city);
			DSUtils.paramToJSONObject(data, "country", country);
			DSUtils.paramToJSONObject(data, "published", published);
			DSUtils.paramToJSONObject(data, "id", idUpdate);

			System.out.println(data);
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			data = DSUtils.insertOrUpdate(action, "shop", data, query);
			final ImagesStore imgStore = new ImagesStore();
			if(main_file_name != null){
				final JSONObject imgResult = new JSONObject(imgStore.insertOrUpdate(action, main_file_name, main_image_data, main_content_type, "shop_main_img", "Y", DSUtils.getValueFromJSON(data, "main_img_ref_id")));
				if("OK".equals(imgResult.getString("result"))){
					data.put("main_img_ref_id", imgResult.getInt("id"));
					DSUtils.insertOrUpdate("update", "shop", data, query);
				}
			}
			if(hl_file_name != null){
				final JSONObject imgResult = new JSONObject(imgStore.insertOrUpdate(action, main_file_name, main_image_data, main_content_type, "shop_hl_img", "Y", DSUtils.getValueFromJSON(data, "hl_img_ref_id")));
				if("OK".equals(imgResult.getString("result"))){
					data.put("hl_img_ref_id", imgResult.getInt("id"));
					DSUtils.insertOrUpdate("update", "shop", data, query);
				}
			}
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }
    
    /**
     * DELETE method for delete instance of slide images
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@QueryParam(value="id")final String id) throws JSONException {
    	final JSONObject response = new JSONObject();
    	try {
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			System.out.println("id = " + id);
			final JSONObject imgRow = DSUtils.get(id, "shop", query);
			if(imgRow != null){
				DSUtils.delete(DSUtils.getValueFromJSON(imgRow, "main_img_ref_id"), "images_store", query);
				DSUtils.delete(DSUtils.getValueFromJSON(imgRow, "hl_img_ref_id"), "images_store", query);
				DSUtils.delete(id, "shop", query);
			}
			
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }

}