package com.eatsonline.services.rest.common;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.eatsonline.common.utils.ConfigUtils;
import com.eatsonline.html.utils.HTMLUtils;
import com.eatsonline.services.db.utils.DSUtils;
import com.eatsonline.services.rest.common.objects.MapObject;

@Path("images_store")
public class ImagesStore extends AbstractRestful {
    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest req;

    /**
     * Default constructor. 
     */
    public ImagesStore() {
    	super();
    }

    /**
     * Retrieves representation of an instance of Users
     * @return an instance of String
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON})
    public ArrayList<MapObject> get(@QueryParam(value="id") final Integer id, @QueryParam(value="published") final String published) {
    	ArrayList<MapObject> result =  null;
    	try {

			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			final ResultSetHandler<ArrayList<MapObject>> handler = new ResultSetHandler<ArrayList<MapObject>>(){
				@Override
				public ArrayList<MapObject> handle(ResultSet rs) throws SQLException {
					final ArrayList<MapObject> list = new ArrayList<MapObject>(); 
					while(rs.next()){
						final MapObject row = new MapObject();
						final int id = rs.getInt("id");
						row.put("seq", list.size() + 1);
						row.put("id", id);
						final ResultSetMetaData rsm = rs.getMetaData();
						int colCount = rsm.getColumnCount();
						for(int i = 1; i <= colCount; i++){
							row.put(rsm.getColumnName(i), rs.getObject(i));
						}
						row.put("file_link", HTMLUtils.createLink(id, "viewImagesStore", "", rs.getString("file_name")) );
						boolean published = "Y".equals(rs.getString("published"));
						row.put("status", published ? "Published" : "Pending");
						row.put("action", 
								HTMLUtils.createImgLink(id, "editImagesStore", "", "images/icon/page_white_edit.png", "Edit") + "&nbsp;" + 
								HTMLUtils.createImgLink(id, "editImagesStore", "", "images/icon/find.png", "View") + "&nbsp;" + 
								HTMLUtils.createImgLink(id, published ? "unPublishImagesStore" :"publishImagesStore", "", published ? "images/icon/cross.png" : "images/icon/accept.png", "Publish"));
						
						list.add(row);
					}
					return list;
				}
			};
			final StringBuffer sql = new StringBuffer();
			sql.append("select * from images_store where 1 = 1 ");
			if(id != null){
				sql.append("and id = ? ");
			}
			if("true".equals(published)){
				sql.append("and published = 'Y' ");
			}
			sql.append("order by id");
			if(id != null){
				result = query.query(sql.toString(), handler, id);
			}else{
				result = query.query(sql.toString(), handler);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    

    /**
     * PUT method for updating or creating an instance of Users
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{action}")
    public String insertOrUpdate(@PathParam(value="action") final String action, 
    		@FormParam(value="file_name")final String file_name,
    		@FormParam(value="file_data")final String file_data,
    		@FormParam(value="content_type")final String content_type, 
    		@FormParam(value="module_name")final String module_name, 
    		@FormParam(value="published")final String published, 
    		@FormParam(value="id")final String idUpdate) throws Exception {
    	final JSONObject response = new JSONObject();
    	try {
    		if(module_name == null || "".equals(module_name)){
    			throw new Exception("module_name cannot be empty value");
    		}
    		
			JSONObject data = new JSONObject();
			DSUtils.paramToJSONObject(data, "content_type", content_type);
			DSUtils.paramToJSONObject(data, "module_name", module_name);
			DSUtils.paramToJSONObject(data, "published", published);
			DSUtils.paramToJSONObject(data, "id", idUpdate);

			System.out.println(data);
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			data = DSUtils.insertOrUpdate(action, "images_store", data, query);
			if(file_name != null){
				final String fileNameArr[] = file_name.split(".");
				final String ext = fileNameArr.length > 0 ? fileNameArr[fileNameArr.length -1] : "jpg";
				final String fileName = module_name + "_" + data.get("id") + "." + ext;
				
				if("insert".equals(action)){
					data.put("file_name", fileName);
					DSUtils.insertOrUpdate("update", "images_store", data, query);
				}
				
				FileOutputStream fos = null;
				try {
					final byte[] img = Base64.decodeBase64(file_data);
					final File folder = new File(ConfigUtils.DOCUMENT_PATH  + File.separator + "images_store" + File.separator  + module_name);
					if(!folder.exists()){
						folder.mkdirs();
					}
					System.out.println("saving file at : " + folder.getAbsolutePath() + File.separator + fileName);
					fos = new FileOutputStream(folder.getAbsolutePath() + File.separator + fileName);
					IOUtils.write(img, fos);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if(fos != null){
						fos.flush();
						fos.close();
					}
				}
			}
 			response.put("id", data.get("id"));
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }
    
    /**
     * DELETE method for delete instance of slide images
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@QueryParam(value="id")final String id) throws JSONException {
    	final JSONObject response = new JSONObject();
    	try {
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			System.out.println("id = " + id);
			DSUtils.delete(id, "slide_images", query);
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }

}