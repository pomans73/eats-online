package com.eatsonline.services.rest.common.objects;

import com.eatsonline.services.rest.common.AbstractJSONData;

public class UsersObject implements AbstractJSONData {

	private int userID;
	private String userName;
	private String password;
	private RoleObject role;
	
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RoleObject getRole() {
		return role;
	}
	public void setRole(RoleObject role) {
		this.role = role;
	}
}
