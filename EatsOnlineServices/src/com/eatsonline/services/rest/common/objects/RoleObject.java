package com.eatsonline.services.rest.common.objects;

import com.eatsonline.services.rest.common.AbstractJSONData;

public class RoleObject implements AbstractJSONData {

	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	private String roleID;
	private String roleName;
	private String permission;
}
