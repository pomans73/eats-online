package com.eatsonline.services.rest.common.objects;

import java.util.HashMap;

import com.eatsonline.services.rest.common.AbstractJSONData;

public class MapObject extends HashMap<String, Object> implements AbstractJSONData {

	private static final long serialVersionUID = -1148836482775130110L;

	public String getString(final String key) {
		return String.valueOf(get(key));
	}
}
