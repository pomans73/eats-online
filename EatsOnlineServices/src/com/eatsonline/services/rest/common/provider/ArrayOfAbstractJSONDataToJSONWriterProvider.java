package com.eatsonline.services.rest.common.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.eatsonline.services.rest.common.AbstractJSONData;

@Provider
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class ArrayOfAbstractJSONDataToJSONWriterProvider extends
		AbstractRestProvider implements
		MessageBodyWriter<ArrayList<AbstractJSONData>> {

	@Context
	UriInfo info;
	@Context
	HttpServletRequest request;

	@Override
	public long getSize(ArrayList<AbstractJSONData> obj, Class<?> clazz,
			Type type, Annotation[] annotation, MediaType mediaType) {
		return -1;
	}

	@Override
	public boolean isWriteable(Class<?> clazz, Type type,
			Annotation[] annotation, MediaType mediaType) {
		return true;
	}

	@Override
	public void writeTo(ArrayList<AbstractJSONData> obj, Class<?> clazz,
			Type type, Annotation[] arg3, MediaType mediaType,
			MultivaluedMap<String, Object> params, OutputStream out)
			throws IOException, WebApplicationException {
		if (!isAuthorizedRequest(request)) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
		
		final MultivaluedMap<String, String> param = info.getQueryParameters();
		final OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8");
		
		if(param.getFirst("page") != null && param.getFirst("rp") != null){ //for flexigrid plugin
			final JSONObject result = new JSONObject();
			try {
				int page = Integer.parseInt(param.getFirst("page"));
				int rp = Integer.parseInt(param.getFirst("rp"));
				int first = ((page -1 ) * rp);
				int last = obj.size() > (page * rp) ? (page * rp) : obj.size();
				result.put("page", param.getFirst("page"));
				result.put("total", obj.size());
				result.put("rows", new org.json.JSONArray(obj.subList(first, last)));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			osw.write(result.toString());
			
		}else if(param.getFirst("jtStartIndex") != null && param.getFirst("jtPageSize") != null){ //for jTable plugin
			final JSONObject result = new JSONObject();
			
			try {
				result.put("Result", "Fail");
				int page = Integer.parseInt(param.getFirst("jtStartIndex")) + 1;
				int rp = Integer.parseInt(param.getFirst("jtPageSize"));
				result.put("Result", "OK");
				result.put("TotalRecordCount", obj.size());
				result.put("Records", obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			osw.write(result.toString());
		}else{
			osw.write(new org.json.JSONArray(obj).toString());
		}
		osw.flush();
		osw.close();
	}

}
