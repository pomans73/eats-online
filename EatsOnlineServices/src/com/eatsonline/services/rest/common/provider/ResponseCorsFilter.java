package com.eatsonline.services.rest.common.provider;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

public class ResponseCorsFilter implements ContainerResponseFilter {

	@Override
	public ContainerResponse filter(ContainerRequest request,
			ContainerResponse response) {

		// add CORS headers
		MultivaluedMap<String, Object> headers = response.getHttpHeaders();
		// allow all origins
		headers.add("Access-Control-Allow-Origin", "*");
		// allow all methods
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		// allow all requested headers
		String requestHeaders = request.getHeaderValue("Access-Control-Request-Headers");
		if (requestHeaders != null) {
			headers.add("Access-Control-Allow-Headers", requestHeaders);
		}

		
		return response;
	}

}