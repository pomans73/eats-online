package com.eatsonline.services.rest.common.provider;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractRestProvider {

	protected static boolean isAuthorizedRequest(final HttpServletRequest request){
		final String host = request.getRemoteAddr();
        return host.equalsIgnoreCase("0:0:0:0:0:0:0:1") || host.equalsIgnoreCase("127.0.0.1");
    }
}
