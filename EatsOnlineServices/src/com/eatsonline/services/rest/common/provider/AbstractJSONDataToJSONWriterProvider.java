package com.eatsonline.services.rest.common.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.MessageBodyWriter;

import com.eatsonline.services.rest.common.AbstractJSONData;

/*@Provider
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON  })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON  })*/
public class AbstractJSONDataToJSONWriterProvider extends AbstractRestProvider
		implements MessageBodyWriter<AbstractJSONData> {

	@Context
	UriInfo info;
	@Context
	HttpServletRequest request;

	@Override
	public long getSize(AbstractJSONData obj, Class<?> clazz, Type type,
			Annotation[] annotation, MediaType mediaType) {
		return -1;
	}

	@Override
	public boolean isWriteable(Class<?> clazz, Type type,
			Annotation[] annotation, MediaType mediaType) {
		
		return true;
	}

	@Override
	public void writeTo(AbstractJSONData obj, Class<?> clazz, Type type,
			Annotation[] annotation, MediaType mediaType,
			MultivaluedMap<String, Object> params, OutputStream out)
			throws IOException, WebApplicationException {
		
		if (!isAuthorizedRequest(request)) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
		
		final MultivaluedMap<String, String> param = info.getQueryParameters();
		final OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8");
		if (param != null && param.get("callback") != null) {
			osw.write((param.getFirst("callback") + "("
					+ org.json.JSONObject.wrap(obj).toString() + ")"));
		} else {
			osw.write(org.json.JSONObject.wrap(obj).toString());
		}
		osw.flush();
		osw.close();

	}

}
