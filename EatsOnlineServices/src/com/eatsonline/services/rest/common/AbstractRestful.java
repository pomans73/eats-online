package com.eatsonline.services.rest.common;

import java.util.ArrayList;

import org.json.JSONException;

import com.eatsonline.services.rest.common.objects.MapObject;

public abstract class AbstractRestful {

	public abstract ArrayList<MapObject> get(final Integer id, final String published);
	public abstract String delete(final String id) throws JSONException;
}
