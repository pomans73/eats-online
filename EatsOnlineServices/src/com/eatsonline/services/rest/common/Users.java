package com.eatsonline.services.rest.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import com.eatsonline.services.db.utils.DSUtils;
import com.eatsonline.services.rest.common.objects.RoleObject;
import com.eatsonline.services.rest.common.objects.UsersObject;

@Path("users")
public class Users {
    @Context
    private UriInfo context;

    /**
     * Default constructor. 
     */
    public Users() {
    	super();
    }

    /**
     * Retrieves representation of an instance of Users
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public UsersObject getJson() {
    	UsersObject user =  null;
    	try {
			
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			user = query.query("select * from user", new ResultSetHandler<UsersObject>(){
				@Override
				public UsersObject handle(ResultSet rs) throws SQLException {
					if(!rs.next()){
						return null;
					}
					UsersObject user = new UsersObject();
			    	user.setUserID(rs.getInt("user_id"));
			    	user.setUserName(rs.getString("username"));
			    	
			    	final RoleObject role = new RoleObject();
			    	role.setRoleID("admin");
			    	role.setRoleName("Adminitrator");
			    	
			    	user.setRole(role);
					
					return user;
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return user;
    }

    /**
     * PUT method for updating or creating an instance of Users
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }

}