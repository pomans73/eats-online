package com.eatsonline.services.rest.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.json.JSONException;
import org.json.JSONObject;

import com.eatsonline.html.utils.HTMLUtils;
import com.eatsonline.services.db.utils.DSUtils;
import com.eatsonline.services.rest.common.objects.MapObject;

@Path("slide_images")
public class SlideImages extends AbstractRestful {
    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest req;

    /**
     * Default constructor. 
     */
    public SlideImages() {
    	super();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayList<MapObject> post(@QueryParam(value="id") final Integer id, @QueryParam(value="published") final String published) {
    	return get(id, published);
    }
    
    @POST
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayList<MapObject> search(@FormParam(value="keyword") final String keyword) {
    	ArrayList<MapObject> result =  null;
    	try {

			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			final ResultSetHandler<ArrayList<MapObject>> handler = new ResultSetHandler<ArrayList<MapObject>>(){
				@Override
				public ArrayList<MapObject> handle(ResultSet rs) throws SQLException {
					final ArrayList<MapObject> list = new ArrayList<MapObject>(); 
					while(rs.next()){
						processResultSet(rs, list);
					}
					return list;
				}

				
			};
			final StringBuffer sql = new StringBuffer();
			sql.append("select * from slide_images where 1 = 1 ");
			if(keyword != null){
				final String keys [] = keyword.split(" ");
				final List<String> params = new ArrayList<String>();
				for(final String key : keys){
					sql.append("and (title like ? or description like ?) ");
					params.add(key + "%");
					params.add(key + "%");
				}
				sql.append("order by id");
				System.out.println(sql);
				System.out.println(params);
				result = query.query(sql.toString(), handler, params.toArray());
			}else{
				sql.append("order by id");
				result = query.query(sql.toString(), handler);
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    
    private void processResultSet(ResultSet rs,
			final ArrayList<MapObject> list) throws SQLException {
		final MapObject row = new MapObject();
		final int id = rs.getInt("id");
		row.put("seq", list.size() + 1);
		final ResultSetMetaData rsm = rs.getMetaData();
		int colCount = rsm.getColumnCount();
		for(int i = 1; i <= colCount; i++){
			row.put(rsm.getColumnName(i), rs.getObject(i));
		}
		row.put("image", HTMLUtils.createImg(rs.getInt("img_ref_id"), "slide_images", "img-thumbnail 140-140", ""));
		boolean published = "Y".equals(rs.getString("published"));
		row.put("published", rs.getString("published"));
		row.put("action", 
				HTMLUtils.createImgLink(id, "editSlide", "", "images/icon/page_white_edit.png", "Edit") + "&nbsp;" + 
				HTMLUtils.createImgLink(id, "viewSlide", "", "images/icon/find.png", "View") + "&nbsp;" + 
				HTMLUtils.createImgLink(id, published ? "unPublishSlide" :"publishSlide", "", published ? "images/icon/cross.png" : "images/icon/accept.png", "Publish"));
		
		list.add(row);
	}
    
    /**
     * Retrieves representation of an instance of Users
     * @return an instance of String
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public ArrayList<MapObject> get(@QueryParam(value="id") final Integer id, @QueryParam(value="published") final String published) {
    	ArrayList<MapObject> result =  null;
    	try {

			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			final ResultSetHandler<ArrayList<MapObject>> handler = new ResultSetHandler<ArrayList<MapObject>>(){
				@Override
				public ArrayList<MapObject> handle(ResultSet rs) throws SQLException {
					final ArrayList<MapObject> list = new ArrayList<MapObject>(); 
					while(rs.next()){
						processResultSet(rs, list);
					}
					return list;
				}
			};
			final StringBuffer sql = new StringBuffer();
			sql.append("select * from slide_images where 1 = 1 ");
			if(id != null){
				sql.append("and id = ? ");
			}
			if("true".equals(published)){
				sql.append("and published = 'Y' ");
			}
			sql.append("order by id");
			if(id != null){
				result = query.query(sql.toString(), handler, id);
			}else{
				result = query.query(sql.toString(), handler);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    

    /**
     * PUT method for updating or creating an instance of Users
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{action}")
    public String insertOrUpdate(@PathParam(value="action") final String action, 
    		@FormParam(value="title")final String title, 
    		@FormParam(value="description")final String description,
    		@FormParam(value="file_name")final String file_name,
    		@FormParam(value="file_data")final String file_data,
    		@FormParam(value="content_type")final String content_type, 
    		@FormParam(value="published")final String published, 
    		@FormParam(value="id")final String idUpdate) throws JSONException {
    	final JSONObject response = new JSONObject();
    	try {
    		
			JSONObject data = new JSONObject();
			DSUtils.paramToJSONObject(data, "title", title);
			DSUtils.paramToJSONObject(data, "description", description);
			DSUtils.paramToJSONObject(data, "published", published);
			DSUtils.paramToJSONObject(data, "id", idUpdate);

			System.out.println(data);
			final DataSource ds = DSUtils.getDataSource();
			final QueryRunner query = new QueryRunner(ds);
			data = DSUtils.insertOrUpdate(action, "slide_images", data, query);
			final ImagesStore imgStore = new ImagesStore();
			if(file_name != null){
				final JSONObject imgResult = new JSONObject(imgStore.insertOrUpdate(action, file_name, file_data, content_type, "slide_images", "Y", DSUtils.getValueFromJSON(data, "img_ref_id")));
				if("OK".equals(imgResult.getString("result"))){
					data.put("img_ref_id", imgResult.getInt("id"));
					DSUtils.insertOrUpdate("update", "slide_images", data, query);
				}
				
			}
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }
    
    /**
     * DELETE method for delete instance of slide images
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     * @throws JSONException 
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@QueryParam(value="id")final String id) throws JSONException {
    	final JSONObject response = new JSONObject();
    	try {
			final DataSource ds = DSUtils.getDataSource();
			Connection con = null;
			con = ds.getConnection();
			con.setAutoCommit(false);
			final QueryRunner query = new QueryRunner(ds);
			final JSONObject imgRow = DSUtils.get(id, "slide_images", query);
			if(imgRow != null){
				DSUtils.delete(id, "slide_images", query);
				DSUtils.delete(DSUtils.getValueFromJSON(imgRow, "img_ref_id"), "images_store", query);
			}
			response.put("result", "OK");
		} catch (Exception e) {
			e.printStackTrace();
			response.put("result", "Fail : "+ e);
		}
    	return response.toString();
    }

}