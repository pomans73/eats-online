package com.eatsonline.services.db.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.json.JSONException;
import org.json.JSONObject;

public class DSUtils {

	public static DataSource getDataSource() throws Exception {
		final InitialContext context = new InitialContext();
		return (DataSource) context.lookup("jdbc/eatsonline");
	}

	public static Integer getGenID(final String table) throws Exception {
		final DataSource ds = DSUtils.getDataSource();
		final QueryRunner query = new QueryRunner(ds);

		final ResultSetHandler<Integer> handler = new ResultSetHandler<Integer>() {
			@Override
			public Integer handle(ResultSet rs) throws SQLException {
				if (rs.next()) {
					return rs.getInt("id") + 1;
				}
				return 1;
			}
		};
		return query.query("select max(id) id from " + table, handler);
	}

	public static JSONObject insertOrUpdate(final String action,
			final String table, final JSONObject data, final QueryRunner query)
			throws Exception, JSONException, SQLException {
		final StringBuffer sql = new StringBuffer();
		final List<Object> params = new ArrayList<Object>();
		int id = -1;

		if (action.equalsIgnoreCase("insert")) {
			sql.append("insert into ").append(table).append("(id");
			id = getGenID(table);
			params.add(id);
			@SuppressWarnings("unchecked")
			final Iterator<String> it = data.keys();
			while (it.hasNext()) {
				final String key = it.next();
				sql.append(",").append(key);
				params.add(data.get(key));
			}

			sql.append(") values (?");
			for (int i = 0; i < params.size() - 1; i++) {
				sql.append(",").append("?");
			}
			sql.append(")");
		} else {
			Object idObj = null;
			sql.append("update ").append(table).append(" set ");
			@SuppressWarnings("unchecked")
			final Iterator<String> it = data.keys();
			while (it.hasNext()) {
				final String key = it.next();
				if (key.equalsIgnoreCase("id")) {
					idObj = data.get(key);
					continue;
				}
				sql.append(key).append(" = ?,");
				params.add(data.get(key));
			}
			if (idObj == null) {
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			} else {
				sql.deleteCharAt(sql.toString().length() - 1);
				sql.append(" where id = ? ");
				params.add(idObj);
				id = Integer.parseInt(String.valueOf(idObj));
			}
		}

		query.update(sql.toString(), params.toArray());
		return query.query("select * from " + table + " where id = ?",
				new ResultSetHandler<JSONObject>() {
					@Override
					public JSONObject handle(ResultSet rs) throws SQLException {
						final JSONObject row = new JSONObject();
						if (rs.next()) {
							final ResultSetMetaData rsm = rs.getMetaData();
							int colCount = rsm.getColumnCount();
							for (int i = 1; i <= colCount; i++) {
								try {
									row.put(rsm.getColumnName(i),
											rs.getObject(i));
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							return row;
						}
						return null;
					}
				}, id);
	}

	public static void delete(final String id, final String table,
			final QueryRunner query) throws SQLException {
		if (id != null) {
			if (id.indexOf(",") != -1) {
				final String[] ids = id.split(",");
				final String[][] params = new String[ids.length][1];
				int i = 0;
				for (final String strID : ids) {
					params[i++][0] = strID;
				}
				query.batch("delete from " + table + " where id = ?", params);
			} else {
				query.update("delete from " + table + " where id = ?", id);
			}
		}
	}

	public static JSONObject get(final String id, final String table,
			final QueryRunner query) throws SQLException {
		if (id != null) {

			return query.query("select * from " + table + " where id = ?",
					new ResultSetHandler<JSONObject>() {

						@Override
						public JSONObject handle(final ResultSet rs) throws SQLException {
							if (rs.next()) {
								final JSONObject row = new JSONObject();
								final ResultSetMetaData rsm = rs.getMetaData();
								int colCount = rsm.getColumnCount();
								for (int i = 1; i <= colCount; i++) {
									try {
										row.put(rsm.getColumnName(i), rs.getObject(i));
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
								return row;
							}
							return null;
						}

					}, id);
		}
		return null;
	}

	public static void paramToJSONObject(final JSONObject data,
			final String fieldName, final Object value) throws Exception {
		if (value != null) {
			data.put(fieldName, value);
		}
	}

	public static String getValueFromJSON(final JSONObject json,
			final String key) throws Exception {
		if (json.has(key)) {
			return String.valueOf(json.get(key));
		}
		return null;
	}
}
