package com.eatsonline.common.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import com.eatsonline.services.db.utils.DSUtils;

public class ConfigUtils {

	private static synchronized Map<String, String> getConfiguration(){
		try {
			final QueryRunner q = new QueryRunner(DSUtils.getDataSource());
			return q.query("select * from configuration", new ResultSetHandler<Map<String, String>>(){
				@Override
				public Map<String, String> handle(ResultSet rs)
						throws SQLException {
					Map<String,String> map = null;
					if(rs.next()){
						map = new HashMap<String, String>();
						final ResultSetMetaData rsm = rs.getMetaData();
						for(int i = 1; i <= rsm.getColumnCount(); i++){
							map.put(rsm.getColumnName(i), rs.getString(i));
						}
					}
					return map;
				}
			});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	private static final Map<String, String> rb = getConfiguration();
	private static String getValue(final String key, final String defaultValue) {
		return rb != null ? rb.get(key) : defaultValue;
	}
	
	
	public static String DOCUMENT_PATH = getValue("docpath", "");
	public static String SERVICE_URI = getValue("service_uri", "");
	
}
