package com.eatsonline.html.utils;

import java.text.MessageFormat;

public class HTMLUtils {

	public static String createImgLink(final int id, final String function, final String className, final String image, final String title) {
		final String src = "<img src=\"{0}\" style=\"cursor:pointer\" onclick=\"{1}({2})\" alt=\"{3}\" title=\"{3}\" class=\"{4}\" border=\"0\"/>";
		return MessageFormat.format(src, image, function, id, title, className);
	}
	
	public static String createLink(final int id, final String function, final String className,  final String title) {
		final String src = "<a href=\"javascript:void(0)\" onclick=\"{0}({1})\" title=\"{2}\" class=\"{3}\">{2}</a>" ;
		return MessageFormat.format(src, function, id, title, className);
	}
	
	public static String createImg(final int id, final String type, final String className, final String title) {
		final String src = "<img src=\"{0}\" alt=\"{1}\" title=\"{1}\" class=\"{2}\"  border=\"0\"/>";
		return MessageFormat.format(src, "/webservice/view_image.bin?id=" + id + "&type=" + type + "&viewImage=true", title, className);
	}
}
