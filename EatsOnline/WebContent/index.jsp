<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="includes/header.jsp" flush="true" />
<style type="text/css">
@import url("css/eatsonline.css");
@import url("css/custom.css");
</style>
</head>
<body>
   
   
   
   
   
   
   
	<header class="head-opac50">
		<span class="logo-section"><img src="images/logo.png" class="logo head-element" alt="" /></span> 
		<span class="account-signup pos-right head-element"> 
			MY ACCOUNT 
			<img
			src="images/line_y.gif" 
			width="150" 
			height="1"
			style="margin-left: 20px; display: block" 
			alt="" 
			class="account-separator-line"/> 
			SIGN UP
		</span> 
		<a href="tel:0800328689"><img src="images/support_number.png" class="support-number pos-right head-element" alt="" /></a>
	</header>
	<div class="slider">
	  <ul class="slides">
	  </ul>
	</div>
	<div id="main-search">
		<div class="img-search">
			<img src="images/hungry.png" width="348" height="125" alt=""  />
		</div>
		<div>
		<form class="form-wrapper cf" style="width: 348px">
			<input type="text" placeholder="Enter your area or restuarant name..." required>
			<button type="submit">Search</button>
		</form>
		</div>
	</div>
	<section class="products">
		<header>
			<h3>
			<span class="special-offer-title">Our Special Offers&nbsp;</span><span class="white-line">&nbsp;</span>
			</h3>
		</header>
		<section class="product-highlight">
			<div class="product-lists"></div>
		</section>
	</section>
	<section class="copy-right">
		<article>Copyright 2013 EatsOnline ltd. All rights reserved</article>

	</section>
	<section class="separator">
		<article class="backgroundwavy_r"></article>
	</section>
	<section class="bottom-menu">
		<article class="menu-category-3cols">
			<header><h3>Browse by cuisine</h3></header>
		
			<nav class="sub-menu-2cols">
				<ul>
					<li><a href="#" class="disabled">Chinese</a></li>
					<li><a href="#" class="disabled">French</a></li>
					<li><a href="#" class="disabled">German</a></li>
					<li><a href="#" class="disabled">Indian</a></li>
					<li><a href="#" class="disabled">Italian</a></li>
					<li><a href="#" class="disabled">Japanese</a></li>
					<li><a href="#" class="disabled">Southern</a></li>
					<li><a href="#" class="disabled">Spanish</a></li>
					<li><a href="#">Thai</a></li>
				</ul>
			</nav>
		</article>

		<article class="menu-category-3cols">
			<header><h3>Browse by dish</h3></header>
			<nav class="sub-menu-2cols">
				<ul>
					<li><a href="#">BBQ Ribs</a></li>
					<li><a href="#">Butter Chicken</a></li>
					<li><a href="#">Chicken Korma</a></li>
					<li><a href="#">Chicken Tikka</a></li>
					<li><a href="#">Green Curry</a></li>
					<li><a href="#">Honey Chicken</a></li>
					<li><a href="#">Honey Chicken</a></li>
					<li><a href="#">Kung Po Honey</a></li>
					<li><a href="#">Lamb Rogan Josh</a></li>
					<li><a href="#">Margherita Pizza</a></li>
					<li><a href="#">Pad Thai</a></li>
					<li><a href="#">Satay Chicken</a></li>
				</ul>
			</nav>
		</article>

		<article class="menu-category-3cols">
			<header><h3>Browse by suburb in your city</h3></header>
			<nav class="sub-menu-2cols">
				<ul>
				<li><a href="#">Adelaode</a></li>
				<li><a href="#">Adelaide</a></li>
				<li><a href="#">Brisbane</a></li>
				<li><a href="#">Canberra</a></li>
				<li><a href="#">Geelong</a></li>
				<li><a href="#">Melbourne</a></li>
				<li><a href="#">Japan</a></li>
				<li><a href="#">Perth</a></li>
				<li><a href="#">Sydney</a></li>
				<li><a href="#">Wollongong</a></li>

				</ul>
			</nav>
		</article>
	</section>
	<section class="bottom-menu">
		<nav class="footer-menu navbar navbar-default" role="navigation">
		<div class="navbar-header">
		    <button type="button"class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		  </div>
		  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul  class="nav navbar-nav">
				<li><span><a href="#">About us</a></span></li>
				<li><span><a href="#">FAQs</a></span></li>
				<li><span><a href="#">Paymeny Options</a></span></li>
				<li><span><a href="#">Delivery Policy</a></span></li>
				<li><span><a href="#">Privacy Statement</a></span></li>
				<li><span><a href="#">Terms &amp; Conditions</a></span></li>
				<li><span><a href="#">Affiliate</a></span></li>
				<li><span><a href="#">Mobile Apps</a></span></li>
				<li><span><a href="#">Contact Us</a></span></li>
			</ul>
			</div>
		</nav>
	</section>
	<footer class="page-footer"> 
		<img src="images/logo_pay.png" width="402" height="39" alt=""/>
	</footer>
	<jsp:include page="includes/footer.jsp" flush="true" />
	<script type="text/javascript" src="js/eatsonline.js"></script>
</body>
</html>