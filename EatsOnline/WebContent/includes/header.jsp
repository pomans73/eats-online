<%@include file="function.jsp" %>

<title>
	EatsOnline : Order Food Online, Food Delivery, Takeaways, Thai Restaurant Guide, Restaurant in Auckland
</title>
<meta content="order food online, eats online, online food ordering,food delivery, takeaways, restaurant in Auckland,thai restaurant nz" name="keywords" >
<meta content="Order Online & Finding Restaurants Guide in New Zealand,Restaurant in Auckland, Food Delivery & Takeaways at Eatsonline." name="description" >
<meta itemprop="name" content="EatsOnline : Order Food Online, Food Delivery, Takeaways, Thai Restaurant Guide, Restaurant in Auckland" >
<meta itemprop="description" content="Order Online & Finding Restaurants Guide in New Zealand,Restaurant in Auckland, Food Delivery & Takeaways at Eatsonline." >

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" >
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=1" >
<meta name="apple-mobile-web-app-capable" content="yes" >
<meta name="format-detection" content="telephone=yes" >
<link rel="icon" type="image/png"  href="images/favicon/76x76.png">
<link rel="apple-touch-icon" href="images/favicon/76x76.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/152x152.png">
<%
includeCSS(application, out, "js/themes/base");
includeCSS(application, out, "jquery.bxslider");
includeCSS(application, out, "add2home/style");
includeCSS(application, out, "bootstrap/css");
%>
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/custom-devices640.css" />
<link rel="stylesheet" type="text/css" href="css/custom-devices480.css" />
<link rel="stylesheet" type="text/css" href="css/searchbox.css" />