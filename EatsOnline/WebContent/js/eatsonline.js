$(document).ready(function() {
	var layer = $('<div id="eats-now" class="yellow-opac55"><div class="shop-detail"></div><div align="center" class="eats-btt"><a href="javascript:void(0)"><img width="90" height="25" border="0" src="images/b_eatnow.png" alt="eat now"/></a></div></div>');
	layer.appendTo('body');
	layer.hover(function() {
		$(this).attr('layer-show', 'true');
	}, function() {
		$(this).css('visibility', 'hidden').removeAttr('layer-show');
	});
	
	
	
	var viewOffer = function(img){
		if (layer.attr('layer-show') && img.attr('has-layer')) {
			return;
		}
		$('.product-lists img.img-menu').removeAttr('has-layer');
		//layer.css('visibility','visible');
		
		layer.css('visibility','visible').hide().fadeIn(100);
		
		layer.attr('layer-show','true');
		layer.width(img.width()).height(img.height());
		$('.shop-detail',layer).html('Dinner Free 8.00 am-9.00<br>Lunch Pay Exter 20%<br><br><br>View Deal');
		$('.eats-btt',layer).css('left',img.width() / 2 - 45);
		var position = img.position();
		layer.css({top : position.top, left : position.left});
		img.attr('has-layer', 'true');
	};
	
	var hideOffer = function(){
		if (layer.attr('layer-show')) {
			return;
		}
		layer.css('visibility','hidden');
		//layer.fadeOut();
		$('.img-menu').removeAttr('has-layer');
	};
	
	
	//load product lists
	var discounts = [10, 15, 20];
	for(var i = 1; i <= 26; i++){
		var item = $('<figure class="item" />').width(236);
		var itemDesc = $('<div class="item-desc" align="center"/>').appendTo(item).width(236);
		$('<img src="images/loader.gif" boder="0" class="loading" style="margin: 20px" alt=""/>').appendTo(itemDesc);
		var img = new Image();
		img.className = 'img-menu';
		img.src = 'images/test' + String((i%13 + 1)) + '.jpg';
		img.alt = '';
		$(img).attr('img-num', i).hide();
		itemDesc.append(img);
		$(img).load(function(){
			var img = $(this).fadeIn();
			var h = this.height;
			var w = this.width;
			var newH = (236 * h) / w;
			this.height = newH;
			console.log('height = ' + newH);
			$('<div class="discount"><span class="discount-text">' + (discounts[img.attr('img-num') % 3])  + '%</span></div>').appendTo(img.parent());
			$('<div class="item-caption"><a href="shop_detail.html?shop_id=1">Uncle and Tuk Tuk Restaurant</a></div>').appendTo(img.parent());
			$('.loading', img.parent()).remove();
			
			$(this).hover(function() {
				viewOffer(img);
			},function() {
				hideOffer();
			});
		});
		
		
		$('.product-lists').append(item);
	}
	
	$('#eats-now').mouseleave(function(e){
		hideOffer();
	});
	var initCols = function() {
		var cols = Math.ceil($(window).width() / 250) -1;
		$('.product-lists').css({
			'column-count' : cols,
			'-moz-column-count' : cols,
			'-webkit-column-count' : cols,
			'width' : (cols * 250) + 'px',
			'min-width' : (cols * 250) + 'px',
		});
		$(layer).css('visibility', 'hidden').removeAttr('layer-show');
	};
	$(window).on('resize', initCols);
	initCols();
	
	$('a.disabled').removeAttr('href');

	
	$.getJSON('http://localhost:8080/webservice/rest/slide_images?published=true', {}, function(rs){
		$.each(rs, function(i, imgData){
			
			var img = new Image();
			img.alt = '';
			img.src = 'http://localhost:8080/webservice/view_image.bin?id='+ imgData.id +'&type=slide_images&viewImage=true';
			var li = $('<li/>');
			$('.slides').append(li);
			li.append(img);
		});
		// slide show control
		$('.slider').flexslider({
			animation: "slide",
			controlNav: false
		});
	});
	
	
	
	
});