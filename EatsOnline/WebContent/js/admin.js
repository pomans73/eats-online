
		$(document).ready(function() {
			$('#nav > li > a').click(function() {
				if ($(this).attr('class') != 'active') {
					$('#nav li ul').slideUp();
					$(this).next().slideToggle();
					$('#nav li a').removeClass('active');
					$(this).addClass('active');
				}
			});
			
			$('a.link', '#nav').click(function(){

				var target = $(this).attr('data-target');
				$('#content-title').text($(this).text());
				$('#content').load('contents/' + target + '.jsp', null, function(responseText, textStatus, XMLHttpRequest){
					
					if(target == 'images-slide'){
						$('#slide-images-table').flexigrid({

							buttons : [
								{name: 'Add', bclass: 'add', onpress : function(){
									alert('add');
								}},
								{name: 'Delete', bclass: 'delete', onpress : function(){
									alert('delete');
								}},
								{separator: true}
								],
							
							usepager: true,
							rp: 15,
							showTableToggleBtn: true,
							width: '100%',
							height: 400
						});   

					}else if(target == 'display-feature-shop'){
						$('#feature-shop-table').flexigrid({

							buttons : [
								{name: 'Add', bclass: 'add', onpress : function(){
									alert('add');
								}},
								{name: 'Delete', bclass: 'delete', onpress : function(){
									alert('delete');
								}},
								{separator: true}
								],
							
							usepager: true,
							rp: 15,
							showTableToggleBtn: true,
							width: '100%',
							height: 400
						});   

					}
					
					
					var editors = ['edit-page-about-us', 'edit-page-draw-prize', 'edit-page-privacy-policy', 'edit-term-conditions'];
					if(editors.indexOf(target) != -1){
						var opts = {
							absoluteURLs: false,
							cssClass : 'el-rte',
							lang     : 'en',
							height   : 420,
							toolbar  : 'maxi'
						};
						$('#editor').elrte(opts);
					}
				});
			});
		});